package sqs

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.{Regions, Region}
import com.amazonaws.services.sqs.AmazonSQSClient
import com.amazonaws.services.sqs.model.{DeleteQueueRequest, CreateQueueRequest}

import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

/**
 * AmazonSQSAsyncClient returns j.u.c.Future which is an abomination. Alternatively you can pass in
 * a AsyncHandler which at least gives you callback semantics, but those cannot be composed.
 *
 * Ideally we'd like to work with s.c.Future here and fortunately the AmazonSQSAsyncClient just
 * extends AmazonSQSClient and delegates calls to its supertype through a backed ExecutorService.
 * This is good news since we can just forgo the use of the AmazonSQSAsyncClient and just interface
 * with the AmazonSQSClient through Scala's future DSL.
 */
object AsynchronousClient extends App {

  // Configure SQS Client
  val sqs = new AmazonSQSClient(new AWSCredentials {
    val getAWSAccessKeyId = "AKIAJCQWOX3QTGWFHJCQ"

    val getAWSSecretKey = "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"
  })

  val usWest2 = Region.getRegion(Regions.US_WEST_2)
  sqs.setRegion(usWest2)

  println("===========================================")
  println("Getting Started with Amazon SQS (Async)")
  println("===========================================\n")

  /*
   * future callbacks
   */

  /*future { sqs.createQueue(new CreateQueueRequest("MyQueue1")) } onComplete {
    case Failure(cause)    => sys.error(cause.toString)
    case Success(response) => // do something with the response
  }*/

  /*
   * future composition
   */

  // create all queues asynchronously
  val createQueues: Seq[Future[String]] = for (i <- 0 to 4) yield future {
    sqs.createQueue(new CreateQueueRequest(s"MyQueue$i")).getQueueUrl
  }

  // let's block until all queues are created. first we want to be working with a
  // Future[Seq[String]] instead of a Seq[Future[String]] so we'll transform that
  // and then block until the result is available
  val urls = Await.result(Future.sequence(createQueues), 1 minute)

  // alternatively we could have attached a callback to Future.sequence(createQueues)
  // instead of explicitly blocking the main thread with Await.

  // print all the urls that were created.
  for (url <- urls) println(s"Created: $url")

  // delete all the queues, but also print activity to the console
  val deleteQueues: Seq[Future[Unit]] = for (url <- urls) yield future {
    println(s"Deleting: $url")
    sqs.deleteQueue(new DeleteQueueRequest(url))
    println(s"Deleted: $url")
  }

  Await.ready(Future.sequence(deleteQueues), 1 minute)

  // Shutdown the client resource
  sqs.shutdown()
}