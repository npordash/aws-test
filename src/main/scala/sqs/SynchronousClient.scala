package sqs

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.regions.{Regions, Region}
import com.amazonaws.services.sqs.AmazonSQSClient
import com.amazonaws.services.sqs.model._

import scala.collection.JavaConverters._

object SynchronousClient extends App {

  // Configure SQS Client
  val sqs = new AmazonSQSClient(new AWSCredentials {
    val getAWSAccessKeyId = "AKIAJCQWOX3QTGWFHJCQ"

    val getAWSSecretKey = "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"
  })

  val usWest2 = Region.getRegion(Regions.US_WEST_2)
  sqs.setRegion(usWest2)

  println("===========================================")
  println("Getting Started with Amazon SQS")
  println("===========================================\n")

  // Create a queue
  println("Creating a new SQS queue called MyQueue.\n")
  val queueUrl = sqs.createQueue(new CreateQueueRequest("MyQueue")).getQueueUrl

  // List queues
  println("Listing all queues in your account.\n")
  for (queue <- sqs.listQueues().getQueueUrls.asScala)
    println(s"QueueUrl: $queue")
  println()

  // Send a message
  println("Sending a message to MyQueue.\n")
  sqs.sendMessage(new SendMessageRequest(queueUrl, "This is my message text."))

  // Receive messages
  println("Receiving messages from MyQueue.\n")
  val messages = sqs.receiveMessage(new ReceiveMessageRequest(queueUrl)).getMessages.asScala

  for (message <- messages) {

    val out =
      s"""  Message
        |     MessageId:     ${message.getMessageId}
        |     ReceiptHandle: ${message.getReceiptHandle}
        |     MD5OfBody:     ${message.getMD5OfBody}
        |     Body:          ${message.getBody}
      """.stripMargin

    println(out)
    println()
  }

  // Delete a message
  println("Deleting a message.\n")
  sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messages(0).getReceiptHandle))

  // Delete a queue
  sqs.deleteQueue(new DeleteQueueRequest(queueUrl))

  // Shutdown the client resource
  sqs.shutdown()
}
